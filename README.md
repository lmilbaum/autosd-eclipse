# AutoSD Eclipse Image

This image contains a specific setup to run packaged eclipse projects in containers within a AutoSD developer image.

Service stack:

* Eclipse Kuksa Databroker
* Eclipse Chariott
* Eclipse Mosquito

Those services are configured to auto-start using bluechi and quadlet so developers can use it their
Eclipse SDV development environment in a container.

## Building

You can build this image by running:

```
podman build --cap-add=sys_admin -t localhost/autosd-eclipse:latest .
```

The `--cap-add=sys_admin` argument is required because the image pulls some
container images during the building process.

## Usage

You can pull the image from quay.io (in case you didn't build it):

```sh
podman pull quay.io/centos-sig-automotive/autosd-eclipse:latest
```

Next step is to run it with privileged mode:

```sh
podman run --name autosd-eclipse-dev -d --privileged quay.io/centos-sig-automotive/autosd-eclipse:latest
```

You can now "enter" into your container and play with it:

```sh
podman exec -it autosd-eclipse-dev /bin/bash
```

## License

[MIT](./LICENSE)
